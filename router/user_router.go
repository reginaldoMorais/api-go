package userrouter

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	. "github.com/reginaldoMorais/api-go/config"
	. "github.com/reginaldoMorais/api-go/config/dao"
	. "github.com/reginaldoMorais/api-go/models"
)

var dao = UsersDAO{}
var config = Config{}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func buildUserResponse(user User) UserReponse {
	config.Read()

	var userReponse UserReponse
	userReponse.ID = user.ID
	userReponse.Name = user.Name
	userReponse.Active = user.Active
	userReponse.Link.Self = "http://" + config.Server + ":8080/api/v1/users/" + user.ID

	return userReponse
}

func GetAll(w http.ResponseWriter, r *http.Request) {
	users, err := dao.GetAll()
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	var userReponses []UserReponse

	for i, user := range users {
		log.Println("User ", i)
		userReponses = append(userReponses, buildUserResponse(user))
	}

	var ur UsersReponse
	ur.Embedded = userReponses
	ur.Link.Self = "http://" + config.Server + ":8080/api/v1/users"

	respondWithJson(w, http.StatusOK, ur)
}

func GetByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	user, err := dao.GetByID(params["id"])
	if err != nil {
		respondWithError(w, http.StatusNotFound, "User Not Found")
		return
	}

	respondWithJson(w, http.StatusOK, buildUserResponse(user))
}

func Create(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	user.ID = uuid.New().String()
	if err := dao.Create(user); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusCreated, buildUserResponse(user))
}

func Update(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	params := mux.Vars(r)
	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := dao.Update(params["id"], user); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusOK, map[string]string{"result": user.Name + " atualizado com sucesso!"})
}

func Delete(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	params := mux.Vars(r)
	if err := dao.Delete(params["id"]); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusNoContent, "")
}
