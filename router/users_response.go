package userrouter

type UsersReponse struct {
	Embedded []UserReponse `bson:"_embedded" json:"_embedded"`
	Link     Link          `bson:"active" json:"link"`
}
