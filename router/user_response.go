package userrouter

type UserReponse struct {
	ID     string `bson:"_id" json:"id"`
	Name   string `bson:"name" json:"name"`
	Active bool   `bson:"active" json:"active"`
	Link   Link   `bson:"active" json:"link"`
}
